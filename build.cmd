@echo off
pushd engine\css
node compile.js
popd

pushd engine\js
call compile.cmd
popd
