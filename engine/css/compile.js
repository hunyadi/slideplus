//
// Minifies CSS files, replacing relative URLs with inline assets
//
// Requirements:
// npm install cssnano
// npm install postcss-assets
//

var postcss = require('postcss');
var assets = require('postcss-assets');
var cssnano = require('cssnano')({
    reduceIdents: false,
    reduceTransforms: false,
    zindex: false
});
var fs = require('fs');

fs.readFile('slideplus.css', 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }

    // postcss-assets recognizes syntax inline(...) for image inlining as base64 but not url(...)
    data = data.replace(/url\(([^()]+)\)/g, 'inline($1)');

    // minify CSS
    postcss([assets, cssnano])
        .process(data, { from: 'slideplus.css', to: 'slideplus.min.css' })
        .then(function (result) {
            fs.writeFile('slideplus.min.css', result.css, function(err) {
                if (err) {
                    return console.log(err);
                }
            });
        });
});
