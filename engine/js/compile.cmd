@echo off
setlocal

if defined JAVA_HOME (
    set JAVA_EXECUTABLE=%JAVA_HOME%\bin\java
) else (
    set JAVA_EXECUTABLE=java
)

set SOURCE=slideplus.js
set TARGET=slideplus.min.js

"%JAVA_EXECUTABLE%" -jar closure-compiler.jar --compilation_level ADVANCED_OPTIMIZATIONS --isolation_mode IIFE --js %SOURCE% --js_output_file %TARGET% --language_in ECMASCRIPT6_STRICT --language_out ECMASCRIPT5_STRICT --warning_level VERBOSE --jscomp_warning=accessControls --jscomp_warning=checkDebuggerStatement --jscomp_warning=checkRegExp --jscomp_warning=constantProperty --jscomp_warning=const --jscomp_warning=deprecatedAnnotations --jscomp_warning=deprecated --jscomp_warning=missingReturn --jscomp_warning=strictModuleDepCheck --jscomp_warning=typeInvalidation --jscomp_warning=undefinedNames --jscomp_warning=visibility

endlocal
